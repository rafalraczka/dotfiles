;;; init.el --- Initialization file for Emacs -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2023, 2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/dotfiles

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(add-to-list
 'load-path (expand-file-name "lisp/config/lisp" user-emacs-directory))

(add-to-list
 'load-path (expand-file-name "lisp/private" user-emacs-directory))

(require 'config-init)

(unless (require 'private-init nil 'noerror)
  (lwarn 'config :warning
         "Private layer of the Emacs configuration is unavailable"))

;;; Footer:

(provide 'init)

;;; init.el ends here
