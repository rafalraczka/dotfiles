export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

if test -d "$XDG_CONFIG_HOME/profile.d"; then
    export PROFILE_DIR="$XDG_CONFIG_HOME/profile.d"
    for profile in "$PROFILE_DIR"/*.sh; do
        test -r "$profile" && . "$profile"
    done
    unset profile
fi
